module.exports = {
    "env": {
        "node": true,
    },
    "extends": [
        "plugin:vue/essential",
        "eslint:recommended",
        "@vue/prettier"
    ],
    "globals": {
        "Atomics": "readonly",
        "SharedArrayBuffer": "readonly"
    },
    "parserOptions": {
        "ecmaVersion": 2018,
        "sourceType": "module",
        "parser": "babel-eslint"
    },
    "plugins": [
        "vue"
    ],
    "rules": {
        "no-unused-vars": "off",
        'prettier/prettier': [
            'off',
            {
                singleQuote: true,
                tabWidth: 4,
            }
        ]
    }
};