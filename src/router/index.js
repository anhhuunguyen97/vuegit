import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const routes = [
    {
        path: '/',
        name: 'login',
        component: () => import('@/views/User/Login.vue'),
    },
    {
        path: '/dashboard',
        name: 'dashboard',
        component: () => '',
    },
    {
        path: '/error-404',
        name: 'error-404',
        component: () => import('@/views/Error.vue'),
    },
    {
        path: '*',
        redirect: 'error-404',
    },
];

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes,
});

export default router;
